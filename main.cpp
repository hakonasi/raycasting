#include <ncurses.h>
#include <string>
#include <cmath>
#include <fstream>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <future>

struct vec{
	vec(float x, float y) : x(x), y(y) {}
	float x = 0.0f;
	float y = 0.0f;
};

struct block {
	block(int x, int y, vec pos) : x(x), y(y), pos(pos){}
	int x = 0;
	int y = 0;
	vec pos;
};

WINDOW* win;
int scrWidth, scrHeight;
int mapWidth = 40, mapHeight = 16;

float renderDistance = 30.0f;
float fov = 1.5f;

const float gravity = 0.08f;
float jumpForce = 0.2f;

float playerX = 10.f;
float playerY = 10.f;
float playerH = 50.0f;
float playerHeightStep = 3.0f;
float walkingSpeed = .2f;
float rotationSpeed = .1f;

float playerAngle = -1.5f;
bool isFalling = false;
bool isJumping = false;
float timeSinceFalling = 0.0f;

// used for moved blocks
float blockHeight = 0.0f;

std::string map;
char shading[] = "@%#k?*+~!=-:\"^`.";

int kbhit (void)
{
  struct timeval tv;
  fd_set rdfs;
 
  tv.tv_sec = 0;
  tv.tv_usec = 0;
 
  FD_ZERO(&rdfs);
  FD_SET (STDIN_FILENO, &rdfs);
 
  select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &rdfs);
 
}

void renderLine(int x){
	float angle = ((float)fov / scrWidth) * (float)x + -fov/2.0f + playerAngle;
	float distance = 0.0f;
	float posX, posY;
	std::vector<std::pair<float, char>> distances;
	int positionOnMap;
	while(distance < renderDistance){
		distance += 0.2f;
		posX = playerX + cos(angle) * distance;
		posY = playerY + sin(angle) * distance;
		if(posX > mapWidth || posX < 0 || posY > mapHeight || posY < 0){
			distances.push_back(std::pair<float, char>(distance, '#'));
			break;
		}
		positionOnMap = mapWidth*(int)posY+(int)posX;
		if(positionOnMap >= map.size()){
			distances.push_back(std::pair<float, char>(distance, '#'));
			break;
		}
		if(map[positionOnMap] != '.'){
			distances.push_back(std::pair<float, char>(distance, map[positionOnMap]));
		}
	}
	std::sort(distances.begin(), distances.end(), [](const std::pair<float, char> &a, const std::pair<float, char> &b) {
		return a.first > b.first;
	});
	for(auto const& [distance, blockSymbol] : distances){
		float b_h = 0.0f;
		int additionalBlockHeight = 0;
		switch(blockSymbol){
			case '1':
				additionalBlockHeight = 1;
				break;
			case '2':
				additionalBlockHeight = 2;
				break;
			case '3':
				additionalBlockHeight = 3;
				break;
			case '4':
				additionalBlockHeight = 4;
				break;
			case '5':
				additionalBlockHeight = 5;
				break;
			case '6':
				additionalBlockHeight = 6;
				break;
			case '7':
				additionalBlockHeight = 7;
				break;
			case '8':
				additionalBlockHeight = 8;
				break;
			case '9':
				additionalBlockHeight = 9;
				break;
			case '0':
				additionalBlockHeight = 10;
				break;
			case 'H':
				b_h = sin(blockHeight) * 8 + 2;
				break;
		}
		int height = (scrHeight / distance);
		int cellHeight = (scrHeight - height) / 2;
		for(int y = cellHeight-additionalBlockHeight-b_h+(playerH*1.0f/distance); y < scrHeight; y++){
			if(y < scrHeight - cellHeight+playerH*1.0f/distance - b_h){
				mvwaddch(win, y, x, shading[(int)(distance)]);
			} else {
				mvwaddch(win, y, x, '.');
			}
		}
	}
}

int main(){
	initscr();
    curs_set(0); // hide cursor
	noecho();
    init_pair(2, COLOR_BLACK, COLOR_GREEN);
    getmaxyx(stdscr, scrHeight, scrWidth);
    win = newwin(scrHeight, scrWidth, 0, 0);

    map += "########################################";
    map += "#..........#...........................#";
    map += "#..........#...........................#";
    map += "#..######..#...........................#";
    map += "#..........#.......1234567890..........#";
    map += "#...........................9..........#";
    map += "#####.........#.............8..........#";
    map += "#...#.......................7..........#";
    map += "#...#.......................6..........#";
    map += "#...#...........H...........5..........#";
    map += "#...#.......................4..........#";
    map += "#...#..####.................5..........#";
    map += "#...#.......................6..........#";
    map += "#...####....................7..........#";
    map += "#.......###............................#";
    map += "#......................................#";
    map += "########################################";
    
    bool isGame = true;
    while(isGame){
		if(kbhit()){
			auto c = getch();
            switch(c){
                case 'w':
					playerX+=cos(playerAngle)*walkingSpeed;
					playerY+=sin(playerAngle)*walkingSpeed;
                    break;
                case 's':
					playerX-=cos(playerAngle)*walkingSpeed;
					playerY-=sin(playerAngle)*walkingSpeed;
                    break;
                case 'a':
					playerX-=-sin(playerAngle)*walkingSpeed;
					playerY-=cos(playerAngle)*walkingSpeed;
                    break;
                case 'd':
					playerX+=-sin(playerAngle)*walkingSpeed;
					playerY+=cos(playerAngle)*walkingSpeed;
                    break;
                case 'e':
					playerAngle+=rotationSpeed;
					break;
                case 'q':
					playerAngle-=rotationSpeed;
					break;
				case 'r':
					playerH+=playerHeightStep;
					break;
				case 'f':
					playerH-=playerHeightStep;
					break;
				case 'z':
					isGame = false;
					break;
				case ' ':
					isJumping = true;
					isFalling = true;
					break;
			}
		}
		if(isJumping){
			playerH+=jumpForce;
		}
		if(isFalling){
			timeSinceFalling+=0.005f;
			playerH -= gravity * timeSinceFalling*timeSinceFalling;
		}

		if(playerH <= 0.0){
			isFalling = false;
			isJumping = false;
			timeSinceFalling = 0.0;
		}
		else{
			isFalling = true;
		}
		
		if(playerX < 0) playerX = 0.0f;
		else if(playerX > mapWidth-1) playerX = mapWidth-1;
		if(playerY < 0) playerY = 0.0f;
		else if(playerY > mapHeight-1) playerY = mapHeight-1;
		if(playerAngle < -6.28f || playerAngle > 6.28f)
			playerAngle = 0.0f;
			
		blockHeight += 0.01f;
		
		werase(win);
		for(int x = 0; x < scrWidth; x++){
#define ASYNC 0
#if ASYNC
			std::async(std::launch::async, renderLine, x);
#else
			renderLine(x);
#endif
		}
		
		for(int y = 0; y <= mapHeight; y++){
			mvwprintw(win, (int)(playerY),(int)(playerX), "P");
			mvwprintw(win, y, 0, map.substr(y*mapWidth, mapWidth).c_str());
		}
		
		for(int y = 0; y < 5; y++){
			mvwprintw(win, (int)playerY + sin(playerAngle) * y, (int)playerX + cos(playerAngle) * y, "5");
		}

		wrefresh(win);
	}
	refresh();
	endwin();
	return 0;
}
