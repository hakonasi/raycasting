# Console Raycasting in C++
A Simple console raycasting implementation. 

<a href="http://www.youtube.com/watch?feature=player_embedded&v=DSvQSY8nsiE" target="_blank">
 <img src="http://img.youtube.com/vi/DSvQSY8nsiE/mqdefault.jpg" alt="Raycasting Showcase" width="480" height="360" border="10" />
</a>

## Compilation

```bash
g++ -lncurses main.cpp -o raycasting
```

## Usage
- Use WASD keys for moving forwards, left, backwards and right
- Q and E for rotating camera
- R and F for changing player's Y position
- Z - exit


## Drawing a Custom Map
You can draw a custom map by editing source code. 
'#' represents a 'solid' block, 'H' is used for moving blocks and numbers are used for making blocks with different heights.
1 is the lowest block and by increasing the number, the height of the block increases. The max value is 0 - the highest block.
